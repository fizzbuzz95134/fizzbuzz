This program generates the first n Fibonacci numbers F(n), and prints

"Buzz" when F(n) is divisible by 3.
"Fizz" when F(n) is divisible by 5.
"FizzBuzz" when F(n) is divisible by 15.
"BuzzFizz" when F(n) is prime.
the value F(n) otherwise.

To compile and run it, run 'make' and then run './src/fib -n 45'.

Run the fib program with no arguments for usage information.  Use the flag '-v'
for verbose output.

The unit tests for the functions fib and isprime will be compiled and run
before the main program.  If any of the unit tests fail the build will fail
and some diagnostic information printed.
