#include "fib.h"

/*
 * Return the n-th fibonacci number.  This is an iterative algorithm that is
 * optimized for space.
 */
int
fib(int n)
{
	int a = 1, b = 1;

	/* We define the fibonacci sequence to include 0 and 1, so handle that
	 * special case here. */
	if (n == 0)
	{
		return n;
	}
	for (int i = 3; i <= n; i++)
	{
		int c = a + b;
		a = b;
		b = c;
	}
	return b;
}
