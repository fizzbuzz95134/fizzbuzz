#ifndef _FIB_H_
#define	_FIB_H_

/*
 * Return the n-th fibonacci number.  The fibonacci sequence is defined as a
 * sequence of numbers where the next number is found by adding up the 2
 * numbers before it, eg: 0, 1, 1, 2, 3, 5, 8, 13, ...
 */
int fib(int n);

#endif
