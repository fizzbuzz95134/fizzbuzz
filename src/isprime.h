#ifndef _ISPRIME_H_
#define	_ISPRIME_H_

/*
 * Return true (1) if the parameter 'n' is prime, false (0) otherwise.
 */
int isprime(int n);

#endif
