#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "fib.h"
#include "isprime.h"

static int usage(const char *cmdname);

/*
 * Driver program for FizzBuzz exercise.
 */
int
main(int ac, char **av)
{
	int any;
	int c;
	const char *cmdname;
	char *end;
	int i;
	int n;
	int verbose;
	int y;

	cmdname = av[0];
	n = 0;
	verbose = 0;
	while ((c = getopt(ac, av, "n:v")) != -1)
	{
		switch (c)
		{
		case 'n':
			end = NULL;
			n = strtoul(optarg, &end, 10);
			if (end == optarg)
			{
				fprintf(stderr, "%s: n invalid\n", cmdname);
				return usage(cmdname);
			}
			break;
		case 'v':
			verbose = 1;
			break;
		default:
			return usage(cmdname);
		}
	}
	if (n == 0)
	{
		return usage(cmdname);
	}
	ac -= optind;
	av += optind;
	if (ac != 0)
	{
		return usage(cmdname);
	}
	if (verbose)
	{
		printf("%s: n=%d\n", cmdname, n);
	}
	for (i = 1; i <= n; i++)
	{
		any = 0;
		y = fib(i);
		if (verbose)
		{
			printf("%s: i=%d fib(%d)=%d\n", cmdname, i, i, y);
		}
		if ((y % 3) == 0)
		{
			printf("Buzz\n");
			any = 1;
		}
		if ((y % 5) == 0)
		{
			printf("Fizz\n");
			any = 1;
		}
		if ((y % 15) == 0)
		{
			printf("FizzBuzz\n");
			any = 1;
		}
		if (isprime(y))
		{
			printf("BuzzFizz\n");
			any = 1;
		}
		if (!any)
		{
			printf("%d\n", y);
		}
	}
	return 0;
}

static int
usage(const char *cmdname)
{
	fprintf(stderr, "usage: %s -n n [-v]\n", cmdname);
	return -1;
}
