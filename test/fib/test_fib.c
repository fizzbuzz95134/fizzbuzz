#include <assert.h>
#include <stdio.h>

#include "fib.h"

int
main(int ac, char **av)
{
	static const int data[] = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144,
		233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368,
		75025, 121393, 196418, 317811 };
	const char *cmdname;
	int i;
	cmdname = av[0];
	for (i = 0; i < sizeof(data) / sizeof(data[0]); i++)
	{
		int y = fib(i);
		if (data[i] != y)
		{
			printf("%s: mismatch i=%d %d != %d\n", cmdname, i, y, data[i]);
			assert(0);
		}
	}
	return 0;
}
